package com.tlz.calendarview

/**
 * Created by Tomlezen.
 * Date: 2017/9/16.
 * Time: 下午5:34.
 */
internal data class TimeWrapper(var year: Int, var month: Int, var day: Int)