package com.tlz.calendarview

/**
 *
 * Created by Tomlezen.
 * Date: 2017/9/15.
 * Time: 17:01.
 */
internal data class Day(val value: Int, var centerX: Float, var centerY: Float, val isReally: Boolean = true, val isToday: Boolean, var isSelected: Boolean = false)