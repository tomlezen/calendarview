package com.tlz.calendarview

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.Align.CENTER
import android.graphics.PointF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.DecelerateInterpolator
import java.sql.Time
import java.util.*

/**
 *
 * Created by Tomlezen.
 * Date: 2017/9/15.
 * Time: 16:03.
 */
class CalendarView(context: Context, attrs: AttributeSet): View(context, attrs) {

  private val paintText = Paint(Paint.ANTI_ALIAS_FLAG)
  private val paintBallBg = Paint(Paint.ANTI_ALIAS_FLAG)

	private var scrollDirect: Int

	private var colorWeekText: Int
  private var colorDayText: Int
  private var colorDaySelectedText: Int
  private var colorDayNoneText: Int
	private var colorBallBg: Int
	private var colorBallSelectedBg: Int
	private var colorBallNoneBg: Int
	private var colorBallTodayBg: Int

	private var sizeWeekText: Float = 0f
		set(value) {
			paintText.textSize = value
			textOffsetY = paintText.fontMetrics.let {
				Math.abs(it.bottom + it.top) / 2
			}
			field = value
		}
	private var sizeWeekBotSpace: Float
  private var sizeDayText: Float = 0f
		set(value) {
			paintText.textSize = sizeWeekText
			weekTextOffsetY = paintText.fontMetrics.let {
				Math.abs(it.bottom + it.top) / 2
			}
			field = value
		}
  private var sizeDayRect: Float

  private var drawnCenterX = 0
  private var drawnCenterY = 0
	private var drawnWeekCenterY = 0f
	private var textOffsetY = 0f
	private var weekTextOffsetY = 0f

	private var touchSlop = ViewConfiguration.get(context).scaledTouchSlop

  private val dayCenters = arrayListOf<PointF>()
  private val days = arrayListOf<Day>()
	private val preDays = arrayListOf<Day>()
	private val nextDays = arrayListOf<Day>()
	private val weekTexts = arrayOf("日", "一", "二", "三", "四", "五", "六")

	private var initMotionX = 0f
	private var initMotionY = 0f

  private val calendar = Calendar.getInstance(Locale.CHINA)
	private val curTime = TimeWrapper(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
	private val preTime = TimeWrapper(0, 0, 0)
	private val nextTime = TimeWrapper(0, 0, 0)
	private val originalTime = TimeWrapper(curTime.year, curTime.month, curTime.day)
	private val selectedTime = TimeWrapper(0, 0, 0)
	private val oldSelectedTime = TimeWrapper(0, 0, 0)

	private val colorEvaluator = ArgbEvaluator()
	private var startTextColor = 0
	private var endTextColor = 0
	private var startBallBgColor = 0
	private var endBallBgColor = 0
	private var progress = 0f
	private var colorProgress = 0f
	private var isCurMonth = true
	private var isAnimRunning = false
	private var isClickAction = false

  init {
    val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CalendarView)
	  scrollDirect = typedArray.getInt(R.styleable.CalendarView_calScrollDirect, HORIENTAL)
    colorWeekText = typedArray.getColor(R.styleable.CalendarView_calWeekTextColor, Color.parseColor("#CCCCCC"))
	  colorDayText = typedArray.getColor(R.styleable.CalendarView_calDayTextColor, Color.parseColor("#6C6C6C"))
    colorDaySelectedText = typedArray.getColor(R.styleable.CalendarView_calDaySelectedTextColor, Color.WHITE)
    colorDayNoneText = typedArray.getColor(R.styleable.CalendarView_calDayNoneTextColor, Color.parseColor("#b5b5b5"))
	  sizeWeekText = typedArray.getDimensionPixelSize(R.styleable.CalendarView_calWeekTextSize, resources.getDimensionPixelSize(R.dimen.def_week_text_size)).toFloat()
	  sizeWeekBotSpace = typedArray.getDimensionPixelSize(R.styleable.CalendarView_calWeekBotSpace, resources.getDimensionPixelSize(R.dimen.def_week_bot_space)).toFloat()
    sizeDayText = typedArray.getDimensionPixelSize(R.styleable.CalendarView_calDayTextSize, resources.getDimensionPixelSize(R.dimen.def_day_text_size)).toFloat()
    sizeDayRect = typedArray.getDimensionPixelSize(R.styleable.CalendarView_calDayRectSize, resources.getDimensionPixelSize(R.dimen.def_day_rect_size)).toFloat()
	  colorBallBg = typedArray.getColor(R.styleable.CalendarView_calDayBgColor, Color.parseColor("#30000000"))
	  colorBallSelectedBg = typedArray.getColor(R.styleable.CalendarView_calDaySelectedBgColor, Color.parseColor("#87CEFF"))
	  colorBallNoneBg = typedArray.getColor(R.styleable.CalendarView_calDayBgColor, Color.parseColor("#1a000000"))
	  colorBallTodayBg = typedArray.getColor(R.styleable.CalendarView_calDayTodayBgColor, Color.parseColor("#FF4040"))
    typedArray.recycle()

    paintText.textAlign = CENTER

    (0 until 42).map { PointF() }.forEach { dayCenters.add(it) }
  }

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec)
		setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec))
	}

	private fun measureWidth(widthMeasureSpec: Int): Int{
		val specMode = View.MeasureSpec.getMode(widthMeasureSpec)
		val specSize = View.MeasureSpec.getSize(widthMeasureSpec)
		return  when(specMode){
			MeasureSpec.EXACTLY -> specSize
			MeasureSpec.UNSPECIFIED, MeasureSpec.AT_MOST -> {
				val size = (sizeDayRect * 7 + 20).toInt()
				if(specMode == MeasureSpec.UNSPECIFIED || size < specSize){
					size
				}else{
					specSize
				}
			}
			else -> specSize
		}
	}

	private fun measureHeight(heightMeasureSpec: Int): Int{
		val specMode = View.MeasureSpec.getMode(heightMeasureSpec)
		val specSize = View.MeasureSpec.getSize(heightMeasureSpec)
		return  when(specMode){
			MeasureSpec.EXACTLY -> specSize
			MeasureSpec.UNSPECIFIED, MeasureSpec.AT_MOST -> {
				val size = (weekTextOffsetY * 2 + sizeWeekBotSpace + 6 * sizeDayRect + 20).toInt()
				if(specMode == MeasureSpec.UNSPECIFIED || size < specSize){
					size
				}else{
					specSize
				}
			}
			else -> specSize
		}
	}

  @SuppressLint("DrawAllocation")
  override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
    super.onLayout(changed, left, top, right, bottom)
    if(changed){
      drawnCenterX = width / 2
      drawnCenterY = height / 2

	    drawnWeekCenterY = weekTextOffsetY

	    val startDrawnDayCenterY = drawnWeekCenterY + weekTextOffsetY + sizeWeekBotSpace + sizeDayRect / 2
      dayCenters.forEachIndexed { index, pointF ->
        val col = (index % 7) - 3
        pointF.x = drawnCenterX + col * sizeDayRect
	      pointF.y = index / 7 * sizeDayRect + startDrawnDayCenterY
      }

      days.clear()
      days.addAll(createData(curTime.year, curTime.month))
	    calculatePreDays()
	    calculateNextDays()
    }
  }

  override fun draw(canvas: Canvas) {
    super.draw(canvas)
	  drawWeek(canvas)
	  drawDays(canvas)
  }

	private fun drawWeek(canvas: Canvas){
		paintText.color = colorWeekText
		paintText.textSize = sizeWeekText
		weekTexts.forEachIndexed { index, s ->
			canvas.drawText(s, dayCenters[index].x, drawnWeekCenterY + weekTextOffsetY, paintText)
		}
	}

	private fun drawDays(canvas: Canvas){
		paintText.textSize = sizeDayText
		days.forEachIndexed { index, value ->
			val centerX: Float
			val centerY: Float
			val textColor: Int
			val bgColor: Int
			calculateColor(value)
			if(progress == 0f){
				centerX = value.centerX
				centerY = value.centerY
				if(colorProgress > 0f && value.isSelected){
					if(value.value == selectedTime.day){
						textColor = colorEvaluator.getColor(colorProgress, if(value.isToday) colorDaySelectedText else colorDayText, startTextColor)
						bgColor = colorEvaluator.getColor(colorProgress, if(value.isToday) colorBallTodayBg else colorBallBg, startBallBgColor)
					}else{
						textColor = colorEvaluator.getColor(colorProgress, startTextColor, if(value.isToday) colorDaySelectedText else colorDayText)
						bgColor = colorEvaluator.getColor(colorProgress, startBallBgColor, if(value.isToday) colorBallTodayBg else colorBallBg)
					}
				}else{
					textColor = startTextColor
					bgColor = startBallBgColor
				}
			}else{
				val preValue = if(progress > 0) preDays[index] else nextDays[index]
				calculateColor(preValue, false)
				val realProgress = Math.abs(progress)
				centerX = preValue.centerX * realProgress + value.centerX * (1 - realProgress)
				centerY = preValue.centerY * realProgress + value.centerY * (1 - realProgress)
				textColor = colorEvaluator.getColor(realProgress, startTextColor, endTextColor)
				bgColor = colorEvaluator.getColor(realProgress, startBallBgColor, endBallBgColor)
			}
			paintText.color = textColor
			paintBallBg.color = bgColor
			canvas.drawCircle(centerX, centerY, sizeDayRect / 2, paintBallBg)
			canvas.drawText(value.value.toString(), centerX, centerY + textOffsetY, paintText)
		}
	}

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent?): Boolean {
	  if(isAnimRunning){
		  return false
	  }
    when(event?.action){
      MotionEvent.ACTION_DOWN -> {
				initMotionX = event.x
	      initMotionY = event.y
	      isClickAction = true
      }
      MotionEvent.ACTION_MOVE -> {
				val diffDis = if(scrollDirect == HORIENTAL) (event.x - initMotionX) else (event.y - initMotionY)
	      if(Math.abs(diffDis) > touchSlop){
		      isClickAction = false
		      progress = diffDis * 2 / (if(scrollDirect == HORIENTAL) width else height)
		      progress = if(progress >= 0) Math.min(1f, progress) else Math.max(-1f, progress)
		      postInvalidate()
	      }else{
		      isClickAction = true
	      }
      }
      MotionEvent.ACTION_UP -> {
	      if(isClickAction){
		      calculateClickPosition(event.x, event.y)
	      }else{
		      calculateRightPosition()
	      }
      }
      MotionEvent.ACTION_CANCEL -> {
	      isClickAction = false
	      progress = 0f
	      postInvalidate()
      }
    }
    return true
  }

	private fun calculateColor(value: Day, isStart: Boolean = true){
		val textColor: Int
		val bgColor: Int
		if(value.isReally){
      when {
        value.isSelected -> {
          textColor = colorDaySelectedText
          bgColor = colorBallSelectedBg
        }
        value.isToday -> {
          textColor = colorDaySelectedText
          bgColor = colorBallTodayBg
        }
        else -> {
          textColor = colorDayText
          bgColor = colorBallBg
        }
      }
		}else{
			textColor = colorDayNoneText
			bgColor = colorBallNoneBg
		}
		if(isStart){
			startTextColor = textColor
			startBallBgColor = bgColor
		}else{
			endTextColor = textColor
			endBallBgColor = bgColor
		}
	}

	private fun calculatePreDays(){
		preDays.addAllAndClear(createData(curTime.year, curTime.month -1))
		preTime.year = calendar.get(Calendar.YEAR)
		preTime.month = calendar.get(Calendar.MONTH)
	}

	private fun calculateNextDays(){
		nextDays.addAllAndClear(createData(curTime.year, curTime.month + 1))
		nextTime.year = calendar.get(Calendar.YEAR)
		nextTime.month = calendar.get(Calendar.MONTH)
	}

  private fun createData(year: Int, month: Int): List<Day>{
    val days = arrayListOf<Day>()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, 1)
	  val isCurMonth = year == originalTime.year && month == originalTime.month
	  val isSelectedMonth = year == selectedTime.year && month == selectedTime.month
	  val dayCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    var week = calendar.get(Calendar.DAY_OF_WEEK) - 1
    (0 until 31).forEach {
      val centerPointF = dayCenters[week]
      days.add(Day(it + 1, centerPointF.x, centerPointF.y, it < dayCount, isCurMonth && it + 1 == originalTime.day, isSelectedMonth && it + 1 == selectedTime.day))
      week++
    }
    return days
  }

	private fun calculateClickPosition(x: Float, y: Float){
		days.forEach {
			if(it.isReally && Math.abs(x - it.centerX) < sizeDayRect / 2 && Math.abs(y - it.centerY) < sizeDayRect / 2 && !it.isSelected){
				it.isSelected = true
				oldSelectedTime.copy(selectedTime)
				selectedTime.year = curTime.year
				selectedTime.month = curTime.month
				selectedTime.day = it.value
				animationToShowSelect()
				return
			}
		}
	}

	private fun animationToShowSelect(){
		isAnimRunning = true
		ObjectAnimator.ofFloat(0f, 1f).apply {
			duration = 500L
			interpolator = DecelerateInterpolator()
			addUpdateListener {
				colorProgress = animatedValue as Float
				invalidate()
			}
			addListener(object: AnimatorListenerAdapter(){
				override fun onAnimationEnd(animation: Animator?) {
					isAnimRunning = false
					colorProgress = 0f
					(when {
            oldSelectedTime.isSameMonth(curTime) -> days
            oldSelectedTime.isSameMonth(preTime) -> preDays
            oldSelectedTime.isSameMonth(nextTime) -> nextDays
            else -> null
          })?.filter {
						it.value == oldSelectedTime.day
					}?.forEach {
						it.isSelected = false
					}
				}
			})
		}.start()
	}

	private fun calculateRightPosition(){
    when {
      progress > 0.4f -> animationToRightPosition(progress, 1f)
      progress < -0.4f -> animationToRightPosition(progress, -1f)
      else -> animationToRightPosition(progress, 0f)
    }
	}

	private fun calculateChange(){
		val temp = arrayListOf<Day>()
		temp.addAll(days)
		if(progress < 0f){
			preTime.copy(curTime)
			curTime.month++
			if(curTime.month > 11){
				curTime.month = 0
				curTime.year++
			}
			days.addAllAndClear(nextDays)
			preDays.addAllAndClear(temp)
			calculateNextDays()
		}else{
			nextTime.copy(curTime)
			curTime.month--
			if(curTime.month < 0){
				curTime.month = 11
				curTime.year--
			}
			days.addAllAndClear(preDays)
			nextDays.addAllAndClear(temp)
			calculatePreDays()
		}
		isCurMonth = curTime.year == originalTime.year && curTime.month == originalTime.month
	}

	private fun animationToRightPosition(start: Float, end: Float){
		isAnimRunning = true
		ObjectAnimator.ofFloat(start, end).apply {
			duration = (400L * Math.abs(start - end)).toLong()
			interpolator = DecelerateInterpolator()
			addUpdateListener {
				progress = animatedValue as Float
				invalidate()
			}
			addListener(object: AnimatorListenerAdapter(){
				override fun onAnimationEnd(animation: Animator?) {
					isAnimRunning = false
					if(Math.abs(progress) == 1f){
						calculateChange()
					}
					progress = 0f
					invalidate()
				}
			})
		}.start()
	}

	private fun <T> MutableList<T>.addAllAndClear(data: List<T>){
		clear()
		addAll(data)
	}

	private fun ArgbEvaluator.getColor(fraction: Float, startColor: Int, endColor: Int): Int{
		return if(startColor == endColor) startColor else evaluate(fraction, startColor, endColor) as Int
	}

	private fun TimeWrapper.copy(wrapper: TimeWrapper){
		year = wrapper.year
		month = wrapper.month
		day = wrapper.day
	}

	private fun TimeWrapper.isSameMonth(wrapper: TimeWrapper) = year == wrapper.year && month == wrapper.month

	companion object {
		val HORIENTAL = 0
		val VERTICAl = 1
	}

}